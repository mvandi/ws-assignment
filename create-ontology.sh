#!/bin/sh

if [[ ! -d Data/CSourceSamples ]]; then
    echo "Extracting Data/CSourceSamples..."
    unzip -q Data/CSourceSamples.zip -d Data
fi

mkdir -p Output

cd Parser

echo "Parsing CSourceSamples..."
./gradlew run --args="../Data/CSourceSamples ../Output"
echo "Generating students..."
./gradlew run --args="G 20 ../Output"
echo "Merging RDF files..."
./gradlew run --args="M ../Ontologies/Syllabus.owl ../Output"

cd ..

mv Output/output.owl Ontologies/
rm -r Output
