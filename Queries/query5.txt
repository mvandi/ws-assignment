# Per ogni libro consigliato in più di un corso, in quali corsi è stato consigliato?

PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX bibtex: <http://purl.org/net/nknouf/ns/bibtex#>
PREFIX : <http://www.semanticweb.org/diego/ontologies/2019/SyllabusV2Merge#>

SELECT ?bookTItle ?courses
WHERE {
    {
        SELECT ?book 
               ( GROUP_CONCAT( ?c; SEPARATOR=", " ) AS ?courses ) 
               ( COUNT ( ?c ) AS ?bookCount )
        WHERE {
            ?c :hasRecommendedResource ?book
        }
        GROUP BY ?book
    }
    FILTER( ?bookCount > 1 ) .
    ?book bibtex:hasTitle ?bookTItle .
}
GROUP BY ?bookTItle ?courses
ORDER BY ?bookTItle
