# Tra tutti i corsi, quante prove sono previste in media prima di poter verbalizzare il voto conseguito?

PREFIX : <http://www.semanticweb.org/diego/ontologies/2019/SyllabusV2Merge#>

SELECT ( AVG( ?numAssessments ) as ?avgAssessments )
WHERE {
    SELECT ?c ( COUNT( ?a ) AS ?numAssessments )
    WHERE {
        ?c :hasAssessmentMethod ?a .
    }
    GROUP BY ?c
}
