package com.gitlab.semanticweb1819.rdf

import org.apache.jena.rdf.model.Property
import org.apache.jena.rdf.model.Resource

object LearnerModelOntology {

    const val NAMESPACE = "http://www.semanticweb.org/diego/ontologies/2019/4/LearnerModelOntology#"

    private infix fun String.resource(resourceName: String): Resource = COntology.model.getResource(this + resourceName)
    private infix fun String.property(propertyName: String): Property = COntology.model.getProperty(this + propertyName)

    val LEARNER = NAMESPACE resource "Learner"

    val IS_ACTIVE = NAMESPACE property "isActive"
    val IS_REFLECTIVE = NAMESPACE property "isReflective"
    val IS_SENSING = NAMESPACE property "isSensing"
    val IS_INTUITIVE = NAMESPACE property "isIntuitive"
    val IS_VISUAL = NAMESPACE property "isVisual"
    val IS_VERBAL = NAMESPACE property "isVerbal"
    val IS_SEQUENTIAL = NAMESPACE property "isSequential"
    val IS_GLOBAL = NAMESPACE property "isGlobal"

}