package com.gitlab.semanticweb1819.rdf

import com.gitlab.semanticweb1819.fileName
import com.gitlab.semanticweb1819.rdf.serialization.mapping.ASTInfoMapping
import com.gitlab.semanticweb1819.rdf.serialization.mapping.ASTInfoMapping.source
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.Property
import org.apache.jena.rdf.model.Resource
import org.apache.jena.rdf.model.ResourceFactory.createStatement
import kotlin.math.log

object Syllabus {

    const val NAMESPACE = "http://www.semanticweb.org/diego/ontologies/2019/SyllabusV2Merge#"

    private infix fun String.resource(resourceName: String): Resource = COntology.model.getResource(this + resourceName)
    private infix fun String.property(propertyName: String): Property = COntology.model.getProperty(this + propertyName)


    val C_SWITCH = NAMESPACE resource "C_Switch"
    val C_ARRAY = NAMESPACE resource "C_Array"
    val C_ENUM = NAMESPACE resource "C_Enum"
    val C_IF = NAMESPACE resource "C_If"
    val C_FOR = NAMESPACE resource "C_For"
    val C_WHILE = NAMESPACE resource "C_While"
    val C_DO = NAMESPACE resource "C_Do"
    val C_FUNCTION = NAMESPACE resource "C_Function"
    val C_LIBRARY = NAMESPACE resource "C_Library"
    val C_STRUCT = NAMESPACE resource "C_Struct"
    val C_POINTER = NAMESPACE resource "C_Pointer"

    val LS_ACTIVE = NAMESPACE resource "Active"
    val LS_REFLECTIVE = NAMESPACE resource "Reflective"
    val LS_SENSING = NAMESPACE resource "Sensing"
    val LS_INTUITIVE = NAMESPACE resource "Intuitive"
    val LS_VISUAL = NAMESPACE resource "Visual"
    val LS_VERBAL = NAMESPACE resource "Verbal"
    val LS_SEQUENTIAL = NAMESPACE resource "Sequential"
    val LS_GLOBAL = NAMESPACE resource "Global"

    val SOURCE = NAMESPACE resource "Source"
    val COURSE = NAMESPACE resource "Course"
    val UNDERGRADUATE_STUDENT = NAMESPACE resource "UndergraduateStudent"
    val HIGHSCHOOL_STUDENT = NAMESPACE resource "HighSchoolStudent"

    val COURSE_C = NAMESPACE resource "Programmazione_C"
    val COURSE_C_2 = NAMESPACE resource "Programmazione_C_2"
    val COURSE_C_SUPERIORI = NAMESPACE resource "Programmazione_C_Superiori"

    val CONCEPT_PROPERTY = NAMESPACE property "hasConcept"
    val LEARNING_OBJECT_PROPERTY = NAMESPACE property "hasLearningObject"
    val LEARNING_STYLE_PROPERTY = NAMESPACE property "hasLearningStyle"
    val LEARNER_PROFILE_PROPERTY = NAMESPACE property "hasLearnerProfile"
    val INTERESTED_IN_PROPERTY = NAMESPACE property "isInterestedIn"


    fun tagLearningStyle(model: Model) {
        var sensing = 0
        var intuitive = 0

        var sequential = 0
        var global = 0

        val lines = model.getProperty(ASTInfoMapping.source, COntology.LINES).int

        // Longer code is for Intuitive, short for Sensing
        if (lines > 400) intuitive += 1 else sensing += 1

        // Files with numbers are usually sorted, better for seq
        if (fileName?.contains("""[0-9]""".toRegex())!!) sequential += 1 else global += 1



        val properties = mutableListOf<Resource>()

        if (sensing != intuitive)
            if (sensing > intuitive) properties.add(LS_SENSING) else properties.add(LS_INTUITIVE)

        // We're tagging written documents
        properties.add(LS_VISUAL)

        if (sequential != global)
            if (sequential > global) properties.add(LS_SEQUENTIAL) else properties.add(LS_GLOBAL)

        properties.forEach { prop ->
            model.add(createStatement(source!!, LEARNING_STYLE_PROPERTY, prop))
        }

    }
}
