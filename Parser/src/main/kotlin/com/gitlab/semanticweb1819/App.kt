package com.gitlab.semanticweb1819

import com.gitlab.semanticweb1819.parser.c.CCodeParser
import com.gitlab.semanticweb1819.parser.c.model.MapperVisitorWithMemory
import com.gitlab.semanticweb1819.rdf.COntology
import com.gitlab.semanticweb1819.rdf.Syllabus
import com.gitlab.semanticweb1819.rdf.serialization.SerializationUtils
import com.gitlab.semanticweb1819.rdf.serialization.mapping.ASTInfoMapping
import mu.KotlinLogging
import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.rdf.model.Resource
import org.apache.jena.rdf.model.ResourceFactory
import org.apache.jena.rdf.model.ResourceFactory.createTypedLiteral
import java.io.File
import java.io.OutputStream
import java.nio.file.Files
import java.nio.file.Paths
import kotlin.system.exitProcess

private val logger = KotlinLogging.logger {}

var fileName: String? = ""
val assignRandomCourse: Boolean = true
val courseName: Resource = Syllabus.COURSE_C

fun main(args: Array<String>) {
    val outputModel = ModelFactory.createDefaultModel()

    if (args[0].isBlank() || args[1].isBlank()) {
        logger.error { "First argument should be the path to the file to parse" }
        logger.error { "Second argument should be the path to the output file" }
        exitProcess(1)
    }

    if (args[0] == "M") {
        Uti.merge(args[1], args[2])
        return
    }

    if (args[0] == "G") {
        Uti.genStudents(args[1].toInt(), args[2])
        return
    }

    val f: List<File> = if (File(args[0]).isDirectory) {
        File(args[0]).listFiles()!!.asList().filter { file -> file.extension == "c" }
    } else {
        listOf(File(args[0]))
    }

    f.forEachIndexed { index, file ->

        fileName = file.name
        val newFileName: String = fileName.toString().split(".").first() + ".rdf"

        try {
            val toParse = try {
                String(Files.readAllBytes(file.toPath()))
//                Files.readString(file.toPath())
            } catch (e: Exception) {
                logger.error(e) { "Cannot read file $fileName" }
                throw e
            }

            val outputStream: OutputStream

            logger.info { "Now parsing $newFileName" }

            if (File(args[1]).isDirectory) {
                outputStream = try {
                    Paths.get(args[1], newFileName).toFile().outputStream()
                } catch (e: Exception) {
                    println(e.message)
                    return
                }

            } else {
                outputStream = try {
                    File(args[1]).outputStream()
                } catch (e: Exception) {
                    println(e.message)
                    exitProcess(1)
                }
            }

            val ast = CCodeParser.parse(toParse)

            ast.translationUnit.accept(
                    MapperVisitorWithMemory(StringBuilder(SerializationUtils.CREATED_RESOURCES_NAMESPACE))
                    { memory, currentNode ->
                        ASTInfoMapping.toRDF(memory, currentNode, outputModel)
                    })


            outputModel.add(ResourceFactory.createStatement(ASTInfoMapping.source, COntology.LINES, createTypedLiteral(toParse.length.toString())))

            Syllabus.tagLearningStyle(outputModel)

            outputModel.setNsPrefixes(COntology.prefixes)
            outputModel.write(outputStream)

            logger.info { "[${index + 1}/${f.size}] Created resulting file at $newFileName" }

        } catch (e: Exception) {
            logger.error { "File $newFileName skipped" }
        }

    }

}
