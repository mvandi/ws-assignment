package com.gitlab.semanticweb1819.parser

import com.gitlab.semanticweb1819.parser.model.ASTNode

/**
 * An interface representing Code Parser abstraction
 */
interface CodeParser {

    /**
     * A function accepting a string for parsing
     */
    fun parse(toParse: String): ASTNode
}