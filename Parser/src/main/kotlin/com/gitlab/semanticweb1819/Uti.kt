package com.gitlab.semanticweb1819

import com.gitlab.semanticweb1819.rdf.LearnerModelOntology
import com.gitlab.semanticweb1819.rdf.LearnerModelOntology.IS_ACTIVE
import com.gitlab.semanticweb1819.rdf.LearnerModelOntology.IS_GLOBAL
import com.gitlab.semanticweb1819.rdf.LearnerModelOntology.IS_INTUITIVE
import com.gitlab.semanticweb1819.rdf.LearnerModelOntology.IS_REFLECTIVE
import com.gitlab.semanticweb1819.rdf.LearnerModelOntology.IS_SENSING
import com.gitlab.semanticweb1819.rdf.LearnerModelOntology.IS_SEQUENTIAL
import com.gitlab.semanticweb1819.rdf.LearnerModelOntology.IS_VERBAL
import com.gitlab.semanticweb1819.rdf.LearnerModelOntology.IS_VISUAL
import com.gitlab.semanticweb1819.rdf.Syllabus
import com.gitlab.semanticweb1819.rdf.Syllabus.C_ARRAY
import com.gitlab.semanticweb1819.rdf.Syllabus.C_DO
import com.gitlab.semanticweb1819.rdf.Syllabus.C_ENUM
import com.gitlab.semanticweb1819.rdf.Syllabus.C_FOR
import com.gitlab.semanticweb1819.rdf.Syllabus.C_FUNCTION
import com.gitlab.semanticweb1819.rdf.Syllabus.C_IF
import com.gitlab.semanticweb1819.rdf.Syllabus.C_LIBRARY
import com.gitlab.semanticweb1819.rdf.Syllabus.C_POINTER
import com.gitlab.semanticweb1819.rdf.Syllabus.C_STRUCT
import com.gitlab.semanticweb1819.rdf.Syllabus.C_SWITCH
import com.gitlab.semanticweb1819.rdf.Syllabus.C_WHILE
import com.gitlab.semanticweb1819.rdf.Syllabus.LS_ACTIVE
import com.gitlab.semanticweb1819.rdf.Syllabus.LS_GLOBAL
import com.gitlab.semanticweb1819.rdf.Syllabus.LS_INTUITIVE
import com.gitlab.semanticweb1819.rdf.Syllabus.LS_REFLECTIVE
import com.gitlab.semanticweb1819.rdf.Syllabus.LS_SENSING
import com.gitlab.semanticweb1819.rdf.Syllabus.LS_SEQUENTIAL
import com.gitlab.semanticweb1819.rdf.Syllabus.LS_VERBAL
import com.gitlab.semanticweb1819.rdf.Syllabus.LS_VISUAL
import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.rdf.model.Property
import org.apache.jena.rdf.model.Resource
import org.apache.jena.rdf.model.ResourceFactory.createResource
import org.apache.jena.vocabulary.RDF
import java.io.File
import java.nio.file.Paths
import kotlin.random.Random
import kotlin.system.exitProcess

object Uti {

    // private const val defaultOntologyURL: String = "D:\\Documenti\\Dropbox\\Uni\\WS - Web Semantico\\Consegna 2-3\\Merge\\c-source-annotator-master\\Ontology\\Syllabus.owl"
    private const val outputFileName: String = "output.owl"
    private val availableCourses = listOf<Resource>(Syllabus.COURSE_C, Syllabus.COURSE_C_2, Syllabus.COURSE_C_SUPERIORI)

    fun merge(ontologyPath: String, outputPath: String) {
        val model = ModelFactory.createDefaultModel()
        model.read(ontologyPath)
        File(outputPath).walk().filter { file -> file.extension == "rdf" }.filter { file -> file.length() > 10 }.forEach {
            model.read(it.canonicalPath)
        }

        val outputStream = try {
            Paths.get(outputPath, outputFileName).toFile().outputStream()
        } catch (e: Exception) {
            println(e.message)
            exitProcess(1)
        }

        model.write(outputStream)
    }

    fun genStudents(n: Int, path: String) {
        val model = ModelFactory.createDefaultModel()

        val names = listOf<String>("Leonardo", "Francesco", "Alessandro", "Lorenzo", "Mattia", "Sofia", "Giulia", "Giorgia",
                "Martina", "Beatrice", "Luca", "Dario", "Sara", "Gaia", "Stefano", "Carla", "Giovanna", "Giovanni", "Andrea", "Stefano", "Mattia",
                "Alessia", "Marzia", "Luisa")

        val surnames = listOf<String>("Bianchi", "Rossi", "Verdi", "Gialli", "Russo", "Costa", "Rizzo", "Conti", "Esposito", "Talamelli",
                "Bianco", "Storti", "Barbieri", "Tagliati", "Omiccioli", "Carnaroli", "Lazzarin", "Marzeri", "Bertarelli", "Abbruciati")

        val subjects = listOf<Resource>(C_SWITCH, C_ARRAY, C_ENUM, C_IF, C_FOR, C_WHILE, C_DO, C_FUNCTION, C_LIBRARY, C_STRUCT, C_POINTER)

        val styles = listOf<Pair<Property, Resource>>(Pair(IS_VISUAL, LS_VISUAL), Pair(IS_GLOBAL, LS_GLOBAL), Pair(IS_ACTIVE, LS_ACTIVE),
                Pair(IS_REFLECTIVE, LS_REFLECTIVE), Pair(IS_SENSING, LS_SENSING), Pair(IS_INTUITIVE, LS_INTUITIVE), Pair(IS_VERBAL, LS_VERBAL),
                Pair(IS_SEQUENTIAL, LS_SEQUENTIAL))

        for (i in 1..n) {
            val name = names.random()
            val surname = surnames.random()
            val student = createResource(Syllabus.NAMESPACE + name + surname)
            val learnerModel = createResource(Syllabus.NAMESPACE + name + surname + "LearnerModel")

            with(model) {
                if (Math.random() > 0.38) {
                    add(createStatement(student, RDF.type, Syllabus.UNDERGRADUATE_STUDENT))
                } else {
                    add(createStatement(student, RDF.type, Syllabus.HIGHSCHOOL_STUDENT))
                }
                for (j in 1..(Random.nextInt(subjects.size) + 1)) {
                    add(student, Syllabus.INTERESTED_IN_PROPERTY, subjects.random())
                }
                add(student, Syllabus.LEARNER_PROFILE_PROPERTY, learnerModel)
                add(createStatement(learnerModel, RDF.type, LearnerModelOntology.LEARNER))
                for (z in 1..(Random.nextInt(styles.size) + 3)) {
                    val index = Random.nextInt(styles.size)
                    add(learnerModel, styles[index].first, styles[index].second)
                }
            }
        }

        val outputStream = try {
            Paths.get(path, "students.rdf").toFile().outputStream()
        } catch (e: Exception) {
            println(e.message)
            exitProcess(1)
        }

        model.write(outputStream)
    }

    fun randomCourse(): Resource {
        return availableCourses.random()
    }
}