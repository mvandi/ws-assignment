package com.gitlab.semanticweb1819.parser.c.model

import com.gitlab.semanticweb1819.parser.c.CCodeParser
import io.kotlintest.matchers.collections.shouldContainAll
import io.kotlintest.specs.StringSpec

/**
 * Tests for AST Visitors
 */
internal class VisitorsTest : StringSpec() {

    private val code = """
        int main(){
            return 0;
        }
        int f(int* p){
            return *p;
        }
    """.trimIndent()

    init {
        "MapperVisitor should visit all nodes"{
            val visitedNodesSource: MutableList<String> = ArrayList()
            CCodeParser.parse(code).translationUnit.accept(MapperVisitor { visitedNodesSource += it.rawSignature })

            visitedNodesSource.shouldContainAll(
                    "main()",
                    "return 0;",
                    "f(int* p)",
                    "return *p;"
            )
        }
    }

}