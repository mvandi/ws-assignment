package com.gitlab.semanticweb1819.parser.c.model

import com.gitlab.semanticweb1819.parser.c.CCodeParser
import io.kotlintest.Spec
import io.kotlintest.inspectors.forAll
import io.kotlintest.matchers.collections.shouldHaveAtLeastSize
import io.kotlintest.matchers.instanceOf
import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec
import org.eclipse.cdt.core.dom.ast.IASTDeclaration
import org.eclipse.cdt.core.dom.ast.IASTFunctionDefinition

/**
 * A class to test [CASTNode]
 */
internal class CASTNodeTest : StringSpec() {

    private val code = """
                        #include<stdio.h>
                        #define f ff
                        int f();
                        int main(){
                            return f() + 5;
                        }
                        int f(){
                            return 5;
                        }
                        """.trimIndent()

    private lateinit var astRoot: CRootASTNode
    override fun beforeSpec(spec: Spec) {
        astRoot = CCodeParser.parse(code)
    }

    init {
        "AST root parent, is null"{
            astRoot.parent shouldBe null
        }

        "AST root children, not empty"{
            astRoot.children shouldHaveAtLeastSize 1
        }

        "AST is correctly determined"{
            val macroDefinition = astRoot.macroDefinitions.firstOrNull()
            macroDefinition?.rawSignature shouldBe "#define f ff"
            macroDefinition?.name?.toString() shouldBe "f"
            macroDefinition?.expansion?.toString() shouldBe "ff"

            val includeDirective = astRoot.includeDirectives.firstOrNull()
            includeDirective?.rawSignature shouldBe "#include<stdio.h>"
            includeDirective?.name?.toString() shouldBe "stdio.h"

            val firstLevelChildren = astRoot.children.map { it as CASTNode }

            firstLevelChildren.forAll { it.eclipseCDTNode shouldBe instanceOf(IASTDeclaration::class) }

            val mainFunDefinition = firstLevelChildren[1].eclipseCDTNode as IASTFunctionDefinition
            mainFunDefinition.declSpecifier.rawSignature shouldBe "int"
            mainFunDefinition.declarator.rawSignature shouldBe "main()"
            mainFunDefinition.body.rawSignature shouldBe """
                                                            {
                                                                return f() + 5;
                                                            }
                                                        """.trimIndent()
        }
    }
}