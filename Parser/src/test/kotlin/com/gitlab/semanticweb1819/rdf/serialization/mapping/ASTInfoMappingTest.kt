package com.gitlab.semanticweb1819.rdf.serialization.mapping

import com.gitlab.semanticweb1819.parser.c.CCodeParser
import com.gitlab.semanticweb1819.parser.c.model.MapperVisitorWithMemory
import com.gitlab.semanticweb1819.rdf.COntology
import com.gitlab.semanticweb1819.rdf.serialization.SerializationUtils
import com.gitlab.semanticweb1819.test.utils.resources.CodeSamples
import io.kotlintest.matchers.collections.shouldContain
import io.kotlintest.matchers.collections.shouldHaveSize
import io.kotlintest.properties.forAll
import io.kotlintest.shouldBe
import io.kotlintest.specs.DescribeSpec
import mu.KotlinLogging
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.rdf.model.Property
import org.apache.jena.rdf.model.RDFNode
import java.nio.file.Files

private val logger = KotlinLogging.logger {}

/**
 * Test class for [ASTInfoMapping]
 */
internal class ASTInfoMappingTest : DescribeSpec() {

    private lateinit var outputRDFModel: Model
    private lateinit var rdfSubjects: List<String>

    private lateinit var subjectToSourceMap: Map<String, String>
    private lateinit var subjectToTypeMap: Map<String, RDFNode>
    private lateinit var subjectToNameMap: Map<String, String>

    private fun defaultMapper() =
            MapperVisitorWithMemory(StringBuilder(SerializationUtils.CREATED_RESOURCES_NAMESPACE))
            { memory, currentNode ->
                ASTInfoMapping.toRDF(memory, currentNode, outputRDFModel)
            }

    /**
     * Initializes the RDf model with provided code RDF serialization
     */
    private fun initializeRdfModelWith(code: String) {
        outputRDFModel = ModelFactory.createDefaultModel()
        CCodeParser.parse(code).translationUnit.accept(defaultMapper())

        rdfSubjects = outputRDFModel.listSubjects().asSequence()
                .map { it.toString().removePrefixURI() }.toList()

        subjectToSourceMap = outputRDFModel.createMapAbout(COntology.SOURCE_CODE_PROPERTY) { it.toString() }
        subjectToTypeMap = outputRDFModel.createMapAbout(COntology.TYPE_PROPERTY) { it.nameOrResource() }
        subjectToNameMap = outputRDFModel.createMapAbout(COntology.NAME_PROPERTY) { it.toString() }
    }

    init {
        describe("Output RDF model") {
            context("with function, directives and variable declaration code") {
                val demoCode = """
                    #include<stdio.h>
                    #define f ff
                    float f(int* param);
                    int main(){
                        int a = 1;
                        return f(&a) + 5;
                    }
                    float f(int* p){
                        return 5;
                    }""".trimIndent()

                initializeRdfModelWith(demoCode)

                val subjectToReturnTypeMap = outputRDFModel.createMapAbout(COntology.RETURN_TYPE_PROPERTY) { it.nameOrResource() }

                logger.debug { rdfSubjects }
                logger.debug { subjectToTypeMap }

                it("should contain macro directive") {
                    rdfSubjects.filter { it.noSuffixNum() == "MacroDefinition" }
                            .apply { shouldHaveSize(1) }
                            .forEach { subjectToSourceMap[it] shouldBe "#define f ff" }
                }
                it("should contain include directive") {
                    rdfSubjects.filter { it.noSuffixNum() == "InclusionStatement" }
                            .apply { shouldHaveSize(1) }
                            .forEach { subjectToSourceMap[it] shouldBe "#include<stdio.h>" }
                }
                it("should contain the function declaration") {
                    rdfSubjects.filter { it.noSuffixNum() == "FunctionDeclarator" }
                            .apply { shouldHaveSize(1) }
                            .forEach {
                                subjectToSourceMap[it] shouldBe "float f(int* param);"
                                subjectToReturnTypeMap[it] shouldBe COntology.FLOAT_INDIVIDUAL
                                subjectToNameMap[it] shouldBe "f"
                            }
                }
                it("should contain the function parameter declaration") {
                    rdfSubjects.filter { it.noSuffixNum() == "ParameterDeclaration" }
                            .apply { shouldHaveSize(2) }
                            .forEach {
                                listOf("int* param" to "param",
                                        "int* p" to "p"
                                ) shouldContain (subjectToSourceMap[it] to subjectToNameMap[it])

                                subjectToTypeMap[it] shouldBe COntology.POINTER_ENTITY
                            }
                }
                it("should contain the two function definitions") {
                    rdfSubjects.filter { it.noSuffixNum() == "FunctionDefinition" }
                            .apply { shouldHaveSize(2) }
                            .forEach {
                                listOf(
                                        Triple("""
                                            int main(){
                                                int a = 1;
                                                return f(&a) + 5;
                                            }
                                            """.trimIndent(), COntology.INT_INDIVIDUAL, "main"),
                                        Triple("""
                                            float f(int* p){
                                                return 5;
                                            }
                                            """.trimIndent(), COntology.FLOAT_INDIVIDUAL, "f")
                                ) shouldContain (Triple(subjectToSourceMap[it], subjectToReturnTypeMap[it], subjectToNameMap[it]))
                            }
                }
                it("should contain a variable declaration") {
                    rdfSubjects.filter { it.noSuffixNum() == "DeclarationStatement" }
                            .apply { shouldHaveSize(1) }
                            .forEach { subjectToSourceMap[it] shouldBe "int a = 1;" }

                    rdfSubjects.filter { it.noSuffixNum() == "LocalVariable" }
                            .apply { shouldHaveSize(1) }
                            .forEach {
                                subjectToNameMap[it] shouldBe "a"
                                subjectToTypeMap[it] shouldBe COntology.INT_INDIVIDUAL
                            }
                }
                it("should contain function invocation") {
                    rdfSubjects.filter { it.noSuffixNum() == "FunctionCallExpression" }
                            .apply { shouldHaveSize(1) }
                            .forEach { subjectToSourceMap[it] shouldBe "f(&a)" }
                }
                it("should contain return statements") {
                    rdfSubjects.filter { it.noSuffixNum() == "ReturnStatement" }
                            .apply { shouldHaveSize(2) }
                            .forEach { listOf("return f(&a) + 5;", "return 5;") shouldContain subjectToSourceMap[it] }
                }
            }


            context("with control flow statements and structured types declarations") {
                val demoCode = """
                    typedef int myInt;
                    typedef struct MyType {
                        int first;
                        double second;
                        char str[10];
                    } MyType;
                    union myUnion{
                        int a;
                        float b[2][2];
                    };
                    typedef union myUnion myUAlias;
                    enum myEnum {
                        FIRST, SECOND, THIRD
                    };
                    int main(){
                        MyType var1;
                        union myUnion var2;
                        enum myEnum var3 = FIRST;
                        for(int i=0; i<5; i++){
                            if(i % 2 == 0){
                                switch(var3){
                                    case FIRST:
                                    case SECOND:
                                        break;
                                    default:
                                        return 1;
                                }
                            } else if (2 == 0){
                                do {
                                    continue;
                                } while(0 == 1);
                            } else {
                                while(i++ % 2 == 1){
                                    var1.str[0] = 'X';
                                    var1.first = (int) sizeof(int);
                                }
                            }
                        }
                    }
                    """.trimIndent()

                initializeRdfModelWith(demoCode)

                val subjectToAliasedType = outputRDFModel.createMapAbout(COntology.ALIASES_PROPERTY) { it.nameOrResource() }
                val subjectToCondition = outputRDFModel.createMapAbout(COntology.CONDITION_PROPERTY) { it.toSourceCode() }

                logger.debug { rdfSubjects }
                logger.debug { subjectToTypeMap }

                it("should contain typedef definitions") {
                    rdfSubjects.filter { it.noSuffixNum() == "TypeAliasDeclaration" }
                            .apply { shouldHaveSize(3) }
                            .forEach {
                                listOf("typedef int myInt;",
                                        """
                                        typedef struct MyType {
                                            int first;
                                            double second;
                                            char str[10];
                                        } MyType;
                                        """.trimIndent(),
                                        "typedef union myUnion myUAlias;"
                                ) shouldContain subjectToSourceMap[it]
                            }

                    rdfSubjects.filter { it.noSuffixNum() == "TypedefNameSpecifier" }
                            .apply { shouldHaveSize(3) }
                            .forEach {
                                listOf("myInt" to COntology.INT_INDIVIDUAL.toString(),
                                        "MyType" to "MyType",
                                        "myUAlias" to "myUnion"
                                ) shouldContain (subjectToNameMap[it] to subjectToAliasedType[it].toString())
                            }
                }
                it("should contain struct declaration") {
                    rdfSubjects.filter { it.noSuffixNum() == "StructTypeDeclaration" }
                            .apply { shouldHaveSize(1) }
                            .forEach {
                                subjectToSourceMap[it] shouldBe """
                                    typedef struct MyType {
                                        int first;
                                        double second;
                                        char str[10];
                                    }""".trimIndent()

                            }
                }
                it("should contain struct an union fields declaration") {
                    rdfSubjects.filter { it.noSuffixNum() == "Field" }
                            .apply { shouldHaveSize(5) }
                            .forEach {
                                listOf("first" to COntology.INT_INDIVIDUAL,
                                        "second" to COntology.DOUBLE_INDIVIDUAL,
                                        "str" to COntology.ARRAY_ENTITY,
                                        "a" to COntology.INT_INDIVIDUAL,
                                        "b" to COntology.ARRAY_ENTITY
                                ) shouldContain (subjectToNameMap[it] to subjectToTypeMap[it])
                            }
                }
                it("should contain union declaration") {
                    rdfSubjects.filter { it.noSuffixNum() == "UnionTypeDeclaration" }
                            .apply { shouldHaveSize(1) }
                            .forEach {
                                subjectToSourceMap[it] shouldBe """
                                union myUnion{
                                    int a;
                                    float b[2][2];
                                }
                            """.trimIndent()
                            }
                }
                it("should contain enum declaration") {
                    rdfSubjects.filter { it.noSuffixNum() == "EnumTypeDeclaration" }
                            .apply { shouldHaveSize(1) }
                            .forEach {
                                subjectToSourceMap[it] shouldBe """
                                enum myEnum {
                                    FIRST, SECOND, THIRD
                                }
                            """.trimIndent()
                            }
                    rdfSubjects.filter { it.noSuffixNum() == "Enumerator" }
                            .apply { shouldHaveSize(3) }
                            .forEach {
                                listOf("FIRST" to "FIRST",
                                        "SECOND" to "SECOND",
                                        "THIRD" to "THIRD"
                                ) shouldContain (subjectToNameMap[it] to subjectToSourceMap[it])
                            }
                }
                it("should contain non primitive type variable declarations") {
                    rdfSubjects.filter { it.noSuffixNum() == "DeclarationStatement" }
                            .apply { shouldHaveSize(4) }
                            .forEach {
                                listOf("MyType var1;",
                                        "union myUnion var2;",
                                        "enum myEnum var3 = FIRST;",
                                        "int i=0;"
                                ) shouldContain subjectToSourceMap[it]
                            }
                    rdfSubjects.filter { it.noSuffixNum() == "LocalVariable" }
                            .apply { shouldHaveSize(4) }
                            .forEach {
                                listOf("var1" to "MyType",
                                        "var2" to "myUnion",
                                        "var3" to "myEnum",
                                        "i" to COntology.INT_INDIVIDUAL.toString()
                                ) shouldContain (subjectToNameMap[it] to subjectToTypeMap[it].toString())
                            }

                }
                it("should contain the for loop") {
                    val subjectToForInit = outputRDFModel.createMapAbout(COntology.FOR_INIT_PROPERTY) { it.toSourceCode() }
                    val subjectToForUpdate = outputRDFModel.createMapAbout(COntology.FOR_UPDATE_PROPERTY) { it.toSourceCode() }
                    rdfSubjects.filter { it.noSuffixNum() == "ForStatement" }
                            .apply { shouldHaveSize(1) }
                            .forEach {
                                subjectToForInit[it] shouldBe "int i=0;"
                                subjectToCondition[it] shouldBe "i<5"
                                subjectToForUpdate[it] shouldBe "i++"
                            }
                }
                it("should contain if statements") {
                    rdfSubjects.filter { it.noSuffixNum() == "IfStatement" }
                            .apply { shouldHaveSize(2) }
                            .forEach { listOf("i % 2 == 0", "2 == 0") shouldContain subjectToCondition[it] }
                }
                it("should contain the switch statement") {
                    rdfSubjects.filter { it.noSuffixNum() == "SwitchStatement" } shouldHaveSize 1
                    rdfSubjects.filter { it.noSuffixNum() == "CaseStatement" } shouldHaveSize 2
                    rdfSubjects.filter { it.noSuffixNum() == "DefaultStatement" } shouldHaveSize 1
                }
                it("should contain do-while statement") {
                    rdfSubjects.filter { it.noSuffixNum() == "DoStatement" }
                            .apply { shouldHaveSize(1) }
                            .forEach { subjectToCondition[it] shouldBe "0 == 1" }
                }
                it("should contain while statement") {
                    rdfSubjects.filter { it.noSuffixNum() == "WhileStatement" }
                            .apply { shouldHaveSize(1) }
                            .forEach { subjectToCondition[it] shouldBe "i++ % 2 == 1" }
                }
                it("should contain a cast") {
                    rdfSubjects.filter { it.noSuffixNum() == "CastExpression" }
                            .apply { shouldHaveSize(1) }
                            .forEach { subjectToTypeMap[it] shouldBe COntology.INT_INDIVIDUAL }
                }
            }
        }

        describe("ASTInfoMapping works") {
            context("foreach correct sample code") {
                it("without throwing exceptions") {
                    forAll(CodeSamples()) { codeSamplePath ->
                        val codeSample = String(Files.readAllBytes(codeSamplePath))
                        logger.debug { "Parsing file: ${codeSamplePath.fileName}" }
                        val translationUnit = CCodeParser.parse(codeSample).translationUnit
                        translationUnit.accept(defaultMapper())
                    }
                }
            }
        }
    }

    private fun String.removePrefixURI() = this.substringAfterLast("/", "")
    private fun String.noSuffixNum() = this.substringBefore(SerializationUtils.SEPARATOR, "")

    /**
     * Creates a map from subject resources names to their objects linked wit provided property;
     *
     * The objects are transformed with provided function
     */
    private fun <T> Model.createMapAbout(property: Property, transformObject: (RDFNode) -> T): Map<String, T> {
        val everyNode: RDFNode? = null
        return this.listStatements(null, property, everyNode).asSequence()
                .associate { it.subject.toString().removePrefixURI() to transformObject(it.`object`) }
    }

    /**
     * Utility function to retrieve the name of a resource if it has one, the resource itself if it is an individual or its rdf-type otherwise
     */
    private fun RDFNode.nameOrResource(): RDFNode {
        val typeName = outputRDFModel.listObjectsOfProperty(this.asResource(), COntology.NAME_PROPERTY)
        val typeType = outputRDFModel.listObjectsOfProperty(this.asResource(), COntology.RDF_TYPE_PROPERTY)
        return when {
            // complex type that has name
            typeName.hasNext() -> typeName.nextNode()

            // individual type resource, (i've not found a better way to distinguish individuals from other resources)
            this.asResource().toString().contains("#") -> this

            // other types return the type resource associated
            else -> typeType.nextNode()
        }
    }

    /**
     * Shorthand function to transform a node to its source code property value
     */
    private fun RDFNode.toSourceCode(): String =
            outputRDFModel.listObjectsOfProperty(this.asResource(), COntology.SOURCE_CODE_PROPERTY).nextNode().toString()
}
