@echo off

MKDIR Output

cd Parser

echo "Parsing CSourceSamples..."
call gradlew run --args="../Data/CSourceSamples ../Output"
echo "Generating students..."
call gradlew run --args="G 50 ../Output/"
call gradlew run --args="G 50 ../Output"
echo "Merging RDF files..."
call gradlew run --args="M ../Ontologies/Syllabus.owl ../Output"

cd ..

MOVE /Y Output\output.owl Ontologies\
RMDIR /S /Q Output
